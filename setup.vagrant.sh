#!/usr/bin/env bash
sudo apt-get update
sudo apt-get install -y git
sudo apt-get install -y libxml2-dev libxslt1-dev python-dev python-lxml python-virtualenv libmysqlclient-dev

sudo debconf-set-selections <<< 'mysql-server mysql-server/root_password password root'
sudo debconf-set-selections <<< 'mysql-server mysql-server/root_password_again password root'
sudo apt-get -y install mysql-server

# Create swap file
sudo dd if=/dev/zero of=/swapfile bs=1024 count=524288
sudo chmod 600 /swapfile
sudo mkswap /swapfile
sudo swapon /swapfile

# Install virtualenvwrapper
sudo pip install virtualenvwrapper
echo "export WORKON_HOME=/home/vagrant/.virtualenvs" >> /home/vagrant/.profile
echo "source /usr/local/bin/virtualenvwrapper.sh" >> /home/vagrant/.profile

# Create database user
Q1="GRANT ALL ON *.* TO 'mysql'@'localhost' IDENTIFIED BY 'mysql';"
Q2="FLUSH PRIVILEGES;"
SQL="${Q1}${Q2}"
mysql -u root -proot -e "$SQL"

sudo sed -i "/skip-external-locking/a skip-character-set-client-handshake\ncharacter-set-server = utf8\ninit-connect='SET NAMES utf8'\ncollation-server=utf8_general_ci" /etc/mysql/my.cnf
sudo sed -i "/mysqldump/a default-character-set=utf8" /etc/mysql/my.cnf
sudo sed -i "/client/a default-character-set=utf8" /etc/mysql/my.cnf
